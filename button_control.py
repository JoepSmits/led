from machine import Pin
from states import States

class ButtonControl(object):

    def __init__(self):
        self.is_pressed = False
        self.button = Pin('C13',Pin.IN).irq(self.callback,Pin.IRQ_FALLING)
        return

    def callback(self,pin):
        self.is_pressed = True
        return

    def button_state_change(self,state):

        if self.is_pressed == True:
            self.is_pressed == False
            if state == States.OFF:
                state = States.RED
            else:
                state = States.OFF

        return (state)