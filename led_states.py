from states import States
from machine import Pin
import random

class LedStates(object):
    
    def __init__(self):
        self.leds = {
            States.RED   : Pin('B0',Pin.OUT),
            States.ORANGE :Pin('E1',Pin.OUT),
            States.GREEN : Pin('E14',Pin.OUT)
        }
    
    def all_off(self):
        self.leds[States.RED].value(0)
        self.leds[States.ORANGE].value(0)
        self.leds[States.GREEN].value(0)

        return

    def toggle_led(self,key):
        if self.leds[key].value() == 1:
            self.leds[key].value(0)
        else:
            self.leds[key].value(1)
        return

    def off(self,last_state,state):
        if last_state != state:
            self.all_off()

        last_state = States.OFF
        return (last_state, state)

    def red(self,last_state,state):
        #Entry action
        if last_state != state:
            print('Wordt nu rood')
            self.all_off()
            last_state = state
            
        self.toggle_led('red')
        print('rood')


        random_number = random.random()
        if random_number <= 0.25:
            state = States.ORANGE
        elif random_number <= 0.5:
            state = States.GREEN

        return (last_state,state)

    def orange(self,last_state,state):

        if last_state != state:
            print('Wordt nu oranje')
            self.all_off()
            last_state = state
            
        self.toggle_led('orange')
        print('oranje')

        random_number = random.random()
        if random_number <= 0.25:
            state = States.RED
        elif random_number <= 0.5:
            state = States.GREEN

        return (last_state,state)

    def green(self,last_state,state):

        if last_state != state:
            print('Wordt nu groen')
            self.all_off()
            last_state = state
            
        self.toggle_led('green')
        print('groen')

        random_number = random.random()
        if random_number <= 0.25:
            state = States.ORANGE
        elif random_number <= 0.5:
            state = States.RED
        
        return (last_state,state)
